package com.boasaude.cadastro.commons.helper;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import com.boasaude.cadastro.commons.utils.JsonUtils;

@Component
public class RestTestHelper
{
	public RestTemplate getRestTemplate()
	{
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://localhost:8080"));
		
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		
		restTemplate.setRequestFactory(requestFactory);
		
		return restTemplate;
	}

	public <T> ResponseEntity<T> callGET(String path, Class<T> responseType) throws JSONException
	{
		HttpHeaders headers = getHeaders();
		
		HttpEntity<String> request = new HttpEntity<String>(headers);
		
		return getRestTemplate().exchange(path, HttpMethod.GET, request, responseType);
	}
	
	public ResponseEntity<String> callPOST(String path, Object body) throws JSONException
	{
		return callPOST(path, body, String.class);
	}
	
	public <T> ResponseEntity<T> callPOST(String path, Object body, Class<T> type) throws JSONException
	{
		HttpHeaders headers = getHeaders();
		String bodyStr = createBody(body);
		
		HttpEntity<String> request = new HttpEntity<String>(bodyStr, headers);
		
		return getRestTemplate().exchange(path, HttpMethod.POST, request, type);
	}

	public ResponseEntity<String> callPUT(String path, Object body) throws JSONException
	{
		return callPUT(path, body, String.class);
	}
	
	public <T> ResponseEntity<T> callPUT(String path, Object body, Class<T> type) throws JSONException
	{
		HttpHeaders headers = getHeaders();
		String bodyStr = createBody(body);
		
		HttpEntity<String> request = new HttpEntity<String>(bodyStr, headers);
		
		return getRestTemplate().exchange(path, HttpMethod.PUT, request, type);
	}
	
	public ResponseEntity<String> callDELETE(String path) throws JSONException
	{
		HttpHeaders headers = getHeaders();
		
		String bodyStr = null;
		
		HttpEntity<String> request = new HttpEntity<String>(bodyStr, headers);
		
		return getRestTemplate().exchange(path, HttpMethod.DELETE, request, String.class);
	}
	
	public ResponseEntity<String> callPATCH(String path) throws JSONException
	{
		HttpHeaders headers = getHeaders();
		
		String bodyStr = null;
		
		HttpEntity<String> request = new HttpEntity<String>(bodyStr, headers);
		
		return getRestTemplate().exchange(path, HttpMethod.PATCH, request, String.class);
	}
	
	private HttpHeaders getHeaders()
	{
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}

	@SuppressWarnings("rawtypes")
	private String createBody(Object body)
	{
		String bodyStr = null;
		
		if (body != null)
		{
			if (body instanceof String || body instanceof JSONObject)
			{
				bodyStr = body.toString();
			}
			else if (body.getClass().isArray())
			{
				bodyStr = new JSONArray(body).toString();
			}
			else if (body instanceof Collection)
			{
				JSONArray bodyArray = new JSONArray();
				
				for (Object object : ((Collection) body))
				{
					bodyArray.put(object);
				}
				
				bodyStr = bodyArray.toString();
			}
			else
			{
				bodyStr = JsonUtils.toString(body);
			}
		}
		
		return bodyStr;
	}
}