package com.boasaude.cadastro.prestador.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boasaude.cadastro.commons.exception.BusinessValidationException;
import com.boasaude.cadastro.prestador.entity.Prestador;
import com.boasaude.cadastro.prestador.repository.PrestadorRepository;

@Service
public class PrestadorService
{
	@Autowired
	private PrestadorRepository dao;
	
	public Prestador get(String cpfCnpj)
	{
		return dao.findById(cpfCnpj).orElse(null);
	}
	
	public Prestador salvar(Prestador prestador, boolean autoCadastro)
	{
		if (prestador.getCpfCnpj() != null)
		{
			Prestador prestadorDB = get(prestador.getCpfCnpj());
			
			if (prestadorDB != null)
			{
				if (autoCadastro)
				{
					throw new BusinessValidationException("cpf ou cnpj já existem");
				}
				
				prestador.setAtivo(prestadorDB.isAtivo());
				prestador.setVersion(prestadorDB.getVersion());
			}
			else
			{
				prestador.setAtivo(false);
			}
		}
		else
		{
			prestador.setAtivo(false);
		}
		
		return dao.save(prestador);
	}

	public Prestador patchStatus(String cpfCnpj, boolean ativo)
	{
		Prestador prestadorDB = dao.findById(cpfCnpj).orElse(null);
		
		if (prestadorDB == null)
		{
			throw new BusinessValidationException("user not exists");
		}
		
		prestadorDB.setAtivo(ativo);
		
		return dao.save(prestadorDB);
	}
	
	public List<Prestador> localizarProximos(double x, double y, double raio, String areaAtuacao)
	{
		return dao.localizarProximos(x, y, raio, areaAtuacao);
	}
}