package com.boasaude.cadastro.commons.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHelper
{
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHelper.class);
    
	@ExceptionHandler(value = { BusinessValidationException.class })
	public ResponseEntity<Object> handleInvalidInputException(BusinessValidationException ex)
	{
		logger.error("Invalid Input Exception: ", ex.toJsonString());

		return new ResponseEntity<Object>(ex.toJsonString(), ex.getHttpStatus());
	}
	
    
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> handleInvalidInputException(Exception ex)
	{
		long code = createTrackingCode();
		
		logger.error("Internal server error! Tracking code: " + code, ex);

		return responserUnexpectedError(code);
	}

	private ResponseEntity<Object> responserUnexpectedError(long code)
	{
		return new ResponseEntity<Object>("{\"errors\":[\"unexpected_error\"], \"trackingCode\": " + code + "}", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private long createTrackingCode()
	{
		return (long) (System.currentTimeMillis() + (Math.random() * 1000));
	}
}