package com.boasaude.cadastro.commons.config.mongodb;

import org.bson.BsonDocument;
import org.bson.BsonString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.stereotype.Component;

@Component
public class StartupApplicationMongoDB implements ApplicationListener<ApplicationReadyEvent>, Ordered
{
	@Autowired
	private MongoDatabaseFactory mongoDbFactory;
	
	@Override
	public int getOrder()
	{
		return 3;
	}
	
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event)
	{
		BsonDocument index = new BsonDocument();
		index.putIfAbsent("enderecosDeAtendimento.location", new BsonString("2dsphere"));
		mongoDbFactory.getMongoDatabase().getCollection("prestador").createIndex(index);
	}
}