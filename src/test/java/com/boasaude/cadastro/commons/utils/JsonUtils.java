package com.boasaude.cadastro.commons.utils;

import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils
{
	public static ObjectMapper objectMapper = new ObjectMapper();
	
	static
	{
		objectMapper.setTimeZone( TimeZone.getDefault() );
	}
	
	public static String toString(Object obj)
	{
		try
		{
			return objectMapper.writeValueAsString(obj);
		}
		catch (JsonProcessingException e)
		{
			throw new RuntimeException(e);
		}
	}
}