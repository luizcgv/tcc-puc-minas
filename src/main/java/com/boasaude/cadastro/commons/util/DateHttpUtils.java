package com.boasaude.cadastro.commons.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHttpUtils
{	
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	private static DateFormat DATE_FORMATER = new SimpleDateFormat(DATE_PATTERN , Locale.forLanguageTag("pt_BR"));
	private static DateFormat DATE_TIME_FORMATER = new SimpleDateFormat(DATE_TIME_PATTERN, Locale.forLanguageTag("pt_BR"));
	
	public static Date parseNoTime(String date)
	{
		try
		{
			return DATE_FORMATER.parse(date);
		}
		catch (ParseException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static String formatNoTime(Date date)
	{
		return DATE_FORMATER.format(date);
	}
	
	public static String format(Date date)
	{
		return DATE_TIME_FORMATER.format(date);
	}
	
	public static boolean isSameDay(Date date1, Date date2)
	{
		return DateHttpUtils.isSameDay(date1, date2);
	}
	
	public static boolean isBeforeHour(Date date, int hour)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY) < hour;
	}
	
    public static Date removeTime(Date date)
    {
        Calendar cal = Calendar.getInstance();
        
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
    }
    
    public static Date addDay(Date date, int day)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date); 
    	c.add(Calendar.DATE, day);
    	
    	return c.getTime();
    }
    
    public static Date addYear(Date date, int year)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date); 
    	c.add(Calendar.YEAR, year);
    	
    	return c.getTime();
    }
	
    public static Date getTodayNoTime()
    {
        return removeTime(new Date());
    }
	
    public static int getCurretYear()
    {
    	return getYear(new Date());
    }
	
    public static int getYear(Date date)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date);
    	
    	return c.get(Calendar.YEAR);
    }
}