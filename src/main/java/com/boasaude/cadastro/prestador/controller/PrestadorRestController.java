package com.boasaude.cadastro.prestador.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.boasaude.cadastro.prestador.entity.Prestador;
import com.boasaude.cadastro.prestador.service.PrestadorService;

@RestController
@RequestMapping(value = "/prestador", produces = "application/json")
public class PrestadorRestController
{
	@Autowired
	private PrestadorService prestadorService;
	
	@GetMapping("/{getCpfCnpj}")
	public Prestador get(@PathVariable(name = "getCpfCnpj") String getCpfCnpj)
	{
		return prestadorService.get(getCpfCnpj);
	}
	
	@PostMapping
	public Prestador salvar(@RequestBody Prestador prestador, @RequestParam(required = false) boolean autoCadastro)
	{
		return prestadorService.salvar(prestador, autoCadastro);
	}
	
	@PatchMapping("/{getCpfCnpj}/status/{ativo}")
	public Prestador patchStatus(@PathVariable(name = "getCpfCnpj") String cpfCnpj, @PathVariable(name = "ativo") boolean ativo)
	{
		return prestadorService.patchStatus(cpfCnpj, ativo);
	}
	
	@GetMapping("/location/point")
	public List<Prestador> localizarProximos(@RequestParam double x, @RequestParam double y, @RequestParam double raio, @RequestParam String areaAtuacao)
	{
		return prestadorService.localizarProximos(x, y, raio, areaAtuacao);
	}
}