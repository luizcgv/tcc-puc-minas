package com.boasaude.cadastro.commons.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

import com.boasaude.cadastro.commons.util.DateHttpUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Pessoa
{
	@Id
	private String cpfCnpj;
	private String nome;
	private String telefone;
	private String email;
	
	@JsonFormat(pattern = DateHttpUtils.DATE_PATTERN, timezone = "America/Sao_Paulo")
	private Date dataNascimento;
	
	@JsonIgnore
	private Endereco endereco = new Endereco();
	
	@Version
	@JsonIgnore
	private Long version;
	
	public Endereco getEndereco()
	{
		return endereco;
	}
	public void setEndereco(Endereco endereco)
	{
		this.endereco = endereco;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getCpfCnpj()
	{
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj)
	{
		this.cpfCnpj = cpfCnpj;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public Date getDataNascimento()
	{
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento)
	{
		this.dataNascimento = dataNascimento;
	}
	public Long getVersion()
	{
		return version;
	}
	public void setVersion(Long version)
	{
		this.version = version;
	}
}