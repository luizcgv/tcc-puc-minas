package com.boasaude.cadastro.prestador.controller;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.bson.BsonDocument;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.http.ResponseEntity;

import com.boasaude.cadastro.commons.entity.Endereco;
import com.boasaude.cadastro.commons.helper.RestTestHelper;
import com.boasaude.cadastro.commons.utils.DateUtils;
import com.boasaude.cadastro.prestador.entity.Prestador;
import com.boasaude.cadastro.prestador.repository.PrestadorRepository;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class TestPrestadorController
{
	@Autowired
	private PrestadorRepository dao;
	
	@Autowired
	private RestTestHelper restHelper;
	
	@Autowired
	private MongoDatabaseFactory mongoDbFactory;
	
	@BeforeEach
    public void beforeEach()
	{
		mongoDbFactory.getMongoDatabase().getCollection("prestador").deleteMany(new BsonDocument());
    }

	@Test
	public void testGet() throws JSONException
	{
		Prestador prestadorDB = dao.save( createPrestadorAstrogildo(true) );
		
		ResponseEntity<Prestador> response = restHelper.callGET("/prestador/" + prestadorDB.getCpfCnpj(), Prestador.class);
		Prestador userResponse = response.getBody();
		
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("16862382071", userResponse.getCpfCnpj());
		assertEquals("Astrogildo Gilber", userResponse.getNome());
		assertEquals("astrogildo@a.com", userResponse.getEmail());
		assertEquals("13/02/1985", DateUtils.formatNoTime(userResponse.getDataNascimento()));
		assertEquals("2122222222", userResponse.getTelefone());

		assertEquals(1, userResponse.getEnderecosDeAtendimento().size());
		
		Endereco enderecoAtendimento = userResponse.getEnderecosDeAtendimento().get(0);
		
		assertEquals("20091005", enderecoAtendimento.getCep());
		assertEquals("Rio de Janeiro", enderecoAtendimento.getCidade());
		assertEquals("andar 11", enderecoAtendimento.getComplemento());
		assertEquals("RJ", enderecoAtendimento.getEstado());
		assertEquals("da quitanda", enderecoAtendimento.getLogradouro());
		assertEquals("199", enderecoAtendimento.getNumero());
		assertEquals("rua", enderecoAtendimento.getTipoLogradouro());
		assertArrayEquals(new double[] {-22.8989345, -43.1813468}, enderecoAtendimento.getLocation());
	}
	
	@Test
	public void testSalvarPrestador() throws JSONException
	{
		Prestador prestadorDB = createPrestadorLucio(true);
		
		ResponseEntity<Prestador> response = restHelper.callPOST("/prestador", prestadorDB, Prestador.class);
		Prestador userResponse = response.getBody();
		
		assertEquals(200, response.getStatusCodeValue());
		
		assertUserLucioValues(userResponse);
		
		assertEquals(false, userResponse.isAtivo()); // status deve-se apenas modificar com a api específica
		
		assertUserLucioValues( dao.findById(userResponse.getCpfCnpj()).orElse(null) );
	}
	
	@Test
	public void testPatchStatus() throws JSONException
	{
		Prestador prestadorDB = dao.save( createPrestadorLucio(false) );
		
		ResponseEntity<Prestador> response = restHelper.callGET("/prestador/" + prestadorDB.getCpfCnpj(), Prestador.class);
		Prestador userResponse = response.getBody();
		
		assertEquals(200, response.getStatusCodeValue());
		
		assertUserLucioValues(userResponse);
		assertEquals(false, userResponse.isAtivo()); // status deve-se apenas modificar com a api específica
		
		
		restHelper.callPATCH("/prestador/" + prestadorDB.getCpfCnpj() + "/status/true");
		
		prestadorDB = dao.findById(prestadorDB.getCpfCnpj()).orElse(null);
		assertUserLucioValues(prestadorDB);
		assertEquals(true, prestadorDB.isAtivo());
		
		
		restHelper.callPATCH("/prestador/" + prestadorDB.getCpfCnpj() + "/status/false");
		
		prestadorDB = dao.findById(prestadorDB.getCpfCnpj()).orElse(null);
		assertUserLucioValues(prestadorDB);
		assertEquals(false, prestadorDB.isAtivo());
	}
	
	@Test
	public void testLocalizarProximos_oneResult() throws JSONException
	{
		dao.save( createEdsonArante(true) );
		dao.save( createPrestadorAstrogildo(true) );
		Prestador lucio = dao.save( createPrestadorLucio(true) );
		
		JSONArray prestadores = getByLocation(-22.9099052,-43.2418497, 10, "psicologia");
		
		assertEquals(1, prestadores.length());
		
		JSONObject prestador = prestadores.getJSONObject(0);

		assertEquals(lucio.getCpfCnpj(), prestador.get("cpfCnpj"));
		assertEquals("Lucio Costa", prestador.get("nome"));
	}
	
	@Test
	public void testLocalizarProximos_twoResults() throws JSONException
	{
		dao.save( createEdsonArante(true) );
		Prestador astrogildo = dao.save( createPrestadorAstrogildo(true) );
		Prestador lucio = dao.save( createPrestadorLucio(true) );
		
		JSONArray prestadores = getByLocation(-22.9099052,-43.2418497, 10000, "psicologia");
		
		assertEquals(2, prestadores.length());
		
		JSONObject prestador = prestadores.getJSONObject(0);

		assertEquals(astrogildo.getCpfCnpj(), prestador.get("cpfCnpj"));
		assertEquals("Astrogildo Gilber", prestador.get("nome"));
		
		prestador = prestadores.getJSONObject(1);

		assertEquals(lucio.getCpfCnpj(), prestador.get("cpfCnpj"));
		assertEquals("Lucio Costa", prestador.get("nome"));
	}
	
	@Test
	public void testLocalizarProximos_noResults_desativados() throws JSONException
	{
		dao.save( createEdsonArante(true) );
		dao.save( createPrestadorAstrogildo(false) );
		dao.save( createPrestadorLucio(false) );
		
		JSONArray prestadores = getByLocation(-22.9099052,-43.2418497, 10000, "psicologia");
		
		assertEquals(0, prestadores.length());
	}

	private JSONArray getByLocation(double x, double y, double raio, String areaAtuacao)
	{
		ResponseEntity<String> response = restHelper.callGET(
			"/prestador/location/point?x=" + x + "&y=" + y + "&raio=" + raio + "&areaAtuacao=" + areaAtuacao,
			String.class
		);
		
		assertEquals(200, response.getStatusCodeValue());
		
		return new JSONArray(response.getBody());
	}

	private Prestador createPrestadorAstrogildo(boolean ativo)
	{
		Prestador prestador = new Prestador();
		
		prestador.setNome("Astrogildo Gilber");
		prestador.setEmail("astrogildo@a.com");
		prestador.setCpfCnpj("16862382071");
		prestador.setDataNascimento(DateUtils.parseNoTime("13/02/1985"));
		prestador.setTelefone("2122222222");
		prestador.setAreaAtuacao("psicologia");
		prestador.setAtivo(ativo);
		
		
		Endereco enderecoAtendimento = new Endereco();
		
		enderecoAtendimento.setCep("20091005");
		enderecoAtendimento.setCidade("Rio de Janeiro");
		enderecoAtendimento.setComplemento("andar 11");
		enderecoAtendimento.setEstado("RJ");
		enderecoAtendimento.setLocation(new double[] {-22.8989345, -43.1813468});
		enderecoAtendimento.setLogradouro("da quitanda");
		enderecoAtendimento.setNumero("199");
		enderecoAtendimento.setTipoLogradouro("rua");
		
		prestador.getEnderecosDeAtendimento().add(enderecoAtendimento);
		
		return prestador;
	}

	private Prestador createPrestadorLucio(boolean ativo)
	{
		Prestador prestador = new Prestador();
		
		prestador.setNome("Lucio Costa");
		prestador.setEmail("lucio@a.com");
		prestador.setCpfCnpj("99236053025");
		prestador.setDataNascimento(DateUtils.parseNoTime("11/08/1976"));
		prestador.setTelefone("2132222223");
		prestador.setAreaAtuacao("psicologia");
		prestador.setAtivo(ativo);
		
		Endereco enderecoAtendimento = new Endereco();
		
		enderecoAtendimento.setCep("20550200");
		enderecoAtendimento.setCidade("Rio de Janeiro");
		enderecoAtendimento.setComplemento("casa 10");
		enderecoAtendimento.setEstado("RJ");
		enderecoAtendimento.setLocation(new double[] {-22.9099051,-43.2418498});
		enderecoAtendimento.setLogradouro("Oito de Dezembro");
		enderecoAtendimento.setNumero("390");
		enderecoAtendimento.setTipoLogradouro("rua");
		
		prestador.getEnderecosDeAtendimento().add(enderecoAtendimento);
		
		return prestador;
	}

	private Prestador createEdsonArante(boolean ativo)
	{
		Prestador prestador = new Prestador();
		
		prestador.setNome("Edson Arante");
		prestador.setEmail("edsonarante@a.com");
		prestador.setCpfCnpj("33144187000156");
		prestador.setDataNascimento(DateUtils.parseNoTime("10/01/1977"));
		prestador.setTelefone("2142242224");
		prestador.setAreaAtuacao("odontologia");
		prestador.setAtivo(ativo);
		
		Endereco enderecoAtendimento = new Endereco();
		
		enderecoAtendimento.setCep("20550200");
		enderecoAtendimento.setCidade("Rio de Janeiro");
		enderecoAtendimento.setComplemento("casa 10");
		enderecoAtendimento.setEstado("RJ");
		enderecoAtendimento.setLocation(new double[] {-22.9099051,-43.2418498});
		enderecoAtendimento.setLogradouro("Oito de Dezembro");
		enderecoAtendimento.setNumero("390");
		enderecoAtendimento.setTipoLogradouro("rua");
		
		prestador.getEnderecosDeAtendimento().add(enderecoAtendimento);
		
		return prestador;
	}

	private void assertUserLucioValues(Prestador userResponse)
	{
		assertEquals("Lucio Costa", userResponse.getNome());
		assertEquals("lucio@a.com", userResponse.getEmail());
		assertEquals("99236053025", userResponse.getCpfCnpj());
		assertEquals("11/08/1976", DateUtils.formatNoTime(userResponse.getDataNascimento()));
		assertEquals("2132222223", userResponse.getTelefone());

		assertEquals(1, userResponse.getEnderecosDeAtendimento().size());
		
		Endereco enderecoAtendimento = userResponse.getEnderecosDeAtendimento().get(0);
		
		assertEquals("20550200", enderecoAtendimento.getCep());
		assertEquals("Rio de Janeiro", enderecoAtendimento.getCidade());
		assertEquals("casa 10", enderecoAtendimento.getComplemento());
		assertEquals("RJ", enderecoAtendimento.getEstado());
		assertEquals("Oito de Dezembro", enderecoAtendimento.getLogradouro());
		assertEquals("390", enderecoAtendimento.getNumero());
		assertEquals("rua", enderecoAtendimento.getTipoLogradouro());
		assertArrayEquals(new double[] {-22.9099051,-43.2418498}, enderecoAtendimento.getLocation());
	}
}