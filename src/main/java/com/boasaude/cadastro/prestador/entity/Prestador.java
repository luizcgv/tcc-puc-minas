package com.boasaude.cadastro.prestador.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.boasaude.cadastro.commons.entity.Endereco;
import com.boasaude.cadastro.commons.entity.Pessoa;

@Document(collection = "prestador")
public class Prestador extends Pessoa
{
    private boolean ativo;
    private String areaAtuacao;
	private List<Endereco> enderecosDeAtendimento = new ArrayList<>();
	private List<Formacao> formacoes = new ArrayList<>();
	
	public List<Endereco> getEnderecosDeAtendimento()
	{
		return enderecosDeAtendimento;
	}

	public void setEnderecosDeAtendimento(List<Endereco> enderecoDeAtendimento)
	{
		this.enderecosDeAtendimento = enderecoDeAtendimento;
	}

	public boolean isAtivo()
	{
		return ativo;
	}

	public void setAtivo(boolean ativo)
	{
		this.ativo = ativo;
	}
	
	public static class Formacao
	{
		private String nomeInstituicao;
		private String nomeCurso;
		private String tipoCurso;
		private int anoConclusao;
		
		public String getNomeInstituicao()
		{
			return nomeInstituicao;
		}
		public void setNomeInstituicao(String nomeInstituicao)
		{
			this.nomeInstituicao = nomeInstituicao;
		}
		public String getNomeCurso()
		{
			return nomeCurso;
		}
		public void setNomeCurso(String nomeCurso)
		{
			this.nomeCurso = nomeCurso;
		}
		public String getTipoCurso()
		{
			return tipoCurso;
		}
		public void setTipoCurso(String tipoCurso)
		{
			this.tipoCurso = tipoCurso;
		}
		public int getAnoConclusao()
		{
			return anoConclusao;
		}
		public void setAnoConclusao(int anoConclusao)
		{
			this.anoConclusao = anoConclusao;
		}
	}

	public List<Formacao> getFormacoes()
	{
		return formacoes;
	}

	public void setFormacoes(List<Formacao> formacoes)
	{
		this.formacoes = formacoes;
	}

	public String getAreaAtuacao()
	{
		return areaAtuacao;
	}

	public void setAreaAtuacao(String areaAtuacao)
	{
		this.areaAtuacao = areaAtuacao;
	}
}
