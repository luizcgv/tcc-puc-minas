package com.boasaude.cadastro.prestador.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.boasaude.cadastro.prestador.entity.Prestador;

public interface PrestadorRepository extends MongoRepository<Prestador, String>
{
	@Query(value = "{ 'enderecosDeAtendimento.location': { $near: { $geometry: { type: 'Point', coordinates: [?0, ?1] }, $maxDistance: ?2 } }, areaAtuacao: ?3, ativo: true }", sort = "{ nome : 1 }")
	List<Prestador> localizarProximos(double x, double y, double radius, String areaAtuacao);
}