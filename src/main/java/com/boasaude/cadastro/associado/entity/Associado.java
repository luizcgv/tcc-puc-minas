package com.boasaude.cadastro.associado.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import com.boasaude.cadastro.commons.entity.Pessoa;

@Document(collection = "associado")
public class Associado extends Pessoa
{
	private Status status;
	private TipoPlano tipoPlano;
	private boolean usaPlanoOdonto;

	public static enum Status
	{
		ATIVO, 
		SUSPENSO, 
		INATIVO;
	}

	public static enum TipoPlano
	{
		ENFERMARIA,
		APARTAMENTO,
		VIP;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public TipoPlano getTipoPlano()
	{
		return tipoPlano;
	}

	public void setTipoPlano(TipoPlano tipoPlano)
	{
		this.tipoPlano = tipoPlano;
	}

	public boolean isUsaPlanoOdonto()
	{
		return usaPlanoOdonto;
	}

	public void setUsaPlanoOdonto(boolean usaPlanoOdonto)
	{
		this.usaPlanoOdonto = usaPlanoOdonto;
	}
}