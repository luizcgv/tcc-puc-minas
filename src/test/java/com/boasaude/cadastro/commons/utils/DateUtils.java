package com.boasaude.cadastro.commons.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils
{
	private static DateFormat DATE_FORMATER = new SimpleDateFormat("dd/MM/yyyy", Locale.forLanguageTag("pt_BR"));
	private static DateFormat DATE_TIME_FORMATER = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.forLanguageTag("pt_BR"));
	private static DateFormat TIME_FORMATER = new SimpleDateFormat("HH:mm", Locale.forLanguageTag("pt_BR"));
	
	private static DateFormat DATE_FORMATER_VALIDATE = new SimpleDateFormat("dd/MM/yyyy");
	
	static
	{
		DATE_FORMATER_VALIDATE.setLenient(false);
	}
	
	public static Date parseNoTime(String date)
	{
		if (date == null)
		{
			return null;
		}
		
		try
		{
			return DATE_FORMATER.parse(date);
		}
		catch (ParseException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static String formatNoTime(Date date)
	{
		if (date == null)
		{
			return null;
		}
		
		return DATE_FORMATER.format(date);
	}
	
	public static String formatTime(Date date)
	{
		if (date == null)
		{
			return null;
		}
		
		return TIME_FORMATER.format(date);
	}
	
	public static String format(Date date)
	{
		if (date == null)
		{
			return null;
		}
		
		return DATE_TIME_FORMATER.format(date);
	}
	
	public static boolean isSameDay(Date date1, Date date2)
	{
		return DateUtils.isSameDay(date1, date2);
	}
	
	public static boolean isBeforeHour(Date date, int hour)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY) < hour;
	}
	
    public static Date removeTime(Date date)
    {
        Calendar cal = Calendar.getInstance();
        
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
    }
    
    public static Date addDay(Date date, int day)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date); 
    	c.add(Calendar.DATE, day);
    	
    	return c.getTime();
    }
    
    public static Date addYear(Date date, int year)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date); 
    	c.add(Calendar.YEAR, year);
    	
    	return c.getTime();
    }
	
    public static Date getTodayNoTime()
    {
        return removeTime(new Date());
    }
	
    public static int getCurretYear()
    {
    	return getYear(new Date());
    }
	
    public static int getYear(Date date)
    {
    	Calendar c = Calendar.getInstance(); 
    	
    	c.setTime(date);
    	
    	return c.get(Calendar.YEAR);
    }
    
	public static boolean isDateValid(String date)
	{
		if (date == null)
		{
			return true;
		}
		
		try
		{
			DATE_FORMATER_VALIDATE.parse(date);
			return true;
		}
		catch (ParseException e)
		{
			return false;
		}
	}
	
	public static String getTimeBetweenFormated(Date dateStart, Date dateEnd)
	{
		long durationInMillis = dateEnd.getTime() - dateStart.getTime();
		long minute = (durationInMillis / (1000 * 60)) % 60;
		long hour = (durationInMillis / (1000 * 60 * 60));

		return String.format("%02d:%02d", hour, minute);
	}
}