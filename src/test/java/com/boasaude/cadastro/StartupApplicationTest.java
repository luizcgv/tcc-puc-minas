package com.boasaude.cadastro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.stereotype.Component;

@Component
public class StartupApplicationTest implements ApplicationListener<ApplicationReadyEvent>, Ordered
{
	@Autowired
	private MongoDatabaseFactory mongoDbFactory;
	
	@Override
	public int getOrder()
	{
		return 1;
	}
	
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event)
	{
		mongoDbFactory.getMongoDatabase().drop();
	}
}